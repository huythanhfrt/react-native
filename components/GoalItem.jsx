import { StyleSheet, View, Text, Pressable } from "react-native";
export const GoalItem = ({ itemData, handleDeleteGoal, id }) => {
  return (
    <View style={styles.goalItem}>
      <Pressable
        onPress={() => {
          handleDeleteGoal(id);
        }}
        android_ripple={{ color: "#210644" }} // android
        style={({ pressed }) => pressed && styles.pressItem} //ios
      >
        <Text style={styles.goalText}>{itemData.item.content}</Text>
      </Pressable>
    </View>
  );
};
const styles = StyleSheet.create({
  goalItem: {
    backgroundColor: "#5a3c88",
    margin: 8,
    borderRadius: 8,
  },
  pressItem: {
    opacity: 0.5,
    // cách này sử dụng cho ios
  },
  goalText: {
    padding: 8,
    color: "white",
  },
});
