import { useState } from "react";
import {
  Button,
  Image,
  Modal,
  StyleSheet,
  TextInput,
  View,
} from "react-native";
export const GoalInput = ({ setGoalList, visible, setVisible }) => {
  const [text, setText] = useState("");
  const handleOnChangeTextInput = (text) => {
    setText(text);
  };
  const handleSubmitGoal = () => {
    const id = new Date();
    setGoalList((currentGoalList) => [
      ...currentGoalList,
      { id: id.getTime(), content: text },
    ]);
    setText("");
    setVisible(false);
    // currentGoalList là giá trị hiện tại của goalList
  };
  return (
    <Modal visible={visible} animationType="slide">
      <View style={styles.inputContainer}>
        <Image
          style={styles.image}
          source={require("../assets/images/goal.png")}
        />
        <TextInput
          onChangeText={handleOnChangeTextInput}
          style={styles.inputElement}
          placeholder="Your goal !"
          value={text}
        />
        <View style={styles.buttonContainer}>
          <View style={styles.button}>
            <Button
              onPress={handleSubmitGoal}
              title="Add goal"
              color="#5e0acc"
            />
          </View>
          <View style={styles.button}>
            <Button
              onPress={() => setVisible(false)}
              title="Cancel"
              color="#f31282"
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#311b6b",
  },
  inputElement: {
    borderWidth: 1,
    borderColor: "#e4d0ff",
    backgroundColor: "#e4d0ff",
    color: "#120438",
    borderRadius: 6,
    padding: 8,
    width: "70%",
    marginBottom: 20,
  },
  buttonContainer: {
    width: "70%",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  button: {
    width: 100,
  },
  image: {
    width: 100,
    height: 100,
    margin: 20,
  },
});
