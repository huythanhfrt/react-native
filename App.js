import { useState } from "react";
import { Button, FlatList, StyleSheet, View } from "react-native";
import { GoalInput } from "./components/GoalInput";
import { GoalItem } from "./components/GoalItem";
import { StatusBar } from "expo-status-bar";
export default function App() {
  const [goalList, setGoalList] = useState([]);
  const [visible, setVisible] = useState(false);
  const handleDeleteGoal = (id) => {
    /*
Lý do cho việc sử dụng hàm lambda để cập nhật trạng thái dạng setState đó là khi ta truyền một hàm lambda vào setState,
React sẽ gọi hàm này để lấy giá trị mới của trạng thái, đồng thời nó cũng sẽ kiểm tra xem giá trị mới này có giống với giá trị 
hiện tại của trạng thái hay không. Nếu giá trị mới giống với giá trị hiện tại, React sẽ không cập nhật giao diện, nhằm tối ưu hiệu năng 
Vì vậy, việc sử dụng hàm lambda giúp đảm bảo rằng React sẽ luôn cập nhật giao diện mỗi khi giá trị của trạng thái thay đổi.
cho ứng dụng của bạn.
    */
    setGoalList((currentGoal) => currentGoal.filter((item) => item.id !== id));
  };
  const handleShowModal = () => {
    setVisible(true);
  };
  return (
    <>
      <StatusBar style="light" />
      <View style={styles.container}>
        <Button
          onPress={handleShowModal}
          title="add new goal"
          color="#5a3c88"
        />
        <GoalInput
          visible={visible}
          goalList={goalList}
          setVisible={setVisible}
          setGoalList={setGoalList}
        />
        <View style={styles.goalListContainer}>
          <FlatList
            data={goalList}
            keyExtractor={(item) => item.id}
            alwaysBounceVertical={false}
            renderItem={(itemData) => {
              return (
                <GoalItem
                  itemData={itemData}
                  id={itemData.item.id}
                  handleDeleteGoal={handleDeleteGoal}
                />
              );
            }}
          ></FlatList>
        </View>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 50,
    flex: 1,
  },

  goalListContainer: {
    flex: 5,
  },
});
